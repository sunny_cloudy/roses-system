package cn.stylefeng.roses.system.utils;

import cn.stylefeng.roses.kernel.jwt.utils.JwtTokenUtil;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.impl.DefaultClaims;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;

/**
 * @program: roses-system
 * @description:
 * @author: Mr.Liu
 * @create: 2020-05-07 10:53
 **/
@Component
public class AccountUtils {
    @Autowired
    private JwtTokenUtil jwtTokenUtil;
    private String Account = "Account";
    /**
     * 获取account
     * @return
     */
    public Long getAccount(String token){
        return Long.parseLong(jwtTokenUtil.getUserIdFromToken(token));
    }
    /**
     * 设置account
     * 这个其实就是个例子：向jwt中写入map数据
     * @return 返回生成的token
     */
    public String setAccount(Long account){
        Map<String,Object> map = new HashMap<>();
        map.put(Account,account);
        Claims claims = new DefaultClaims(map);
        return jwtTokenUtil.generateToken(account.toString(), claims);
    }
}
