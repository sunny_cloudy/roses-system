/**
 * Copyright 2018-2020 stylefeng & fengshuonan (sn93@qq.com)
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.stylefeng.roses.system.modular.service;

import cn.stylefeng.roses.core.util.ToolUtil;
import cn.stylefeng.roses.kernel.jwt.utils.JwtTokenUtil;
import cn.stylefeng.roses.kernel.model.api.base.AppUserWarpper;
import cn.stylefeng.roses.kernel.model.api.base.RpcBaseResponse;
import cn.stylefeng.roses.kernel.model.auth.SysUser;
import cn.stylefeng.roses.kernel.model.enums.SystemMenu;
import cn.stylefeng.roses.kernel.model.exception.ServiceException;
import cn.stylefeng.roses.system.api.context.LoginUser;
import cn.stylefeng.roses.system.api.exception.enums.AuthExceptionEnum;
import cn.stylefeng.roses.system.core.constants.SystemConstants;

import cn.stylefeng.roses.system.modular.mapper.SysUserMapper;
import cn.stylefeng.roses.system.utils.AccountUtils;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.BoundValueOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

/**
 * <p>
 * 用户表 服务实现类
 * </p>
 *
 * @author stylefeng
 * @since 2018-08-26
 */
@Service
public class SysUserService extends ServiceImpl<SysUserMapper, SysUser> {
    /**
     * //注意月份是MM
     * 自定义格式化:
     */
    private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
    //Date date = simpleDateFormat.parse(appLogin.getBirthday());
    public static Map<String,String> getMap(String s){
        Map<String,String> m = new HashMap<>();
        m.put("message",s);
        return m;
    }
    @Autowired
    private JwtTokenUtil jwtTokenUtil;
    @Autowired
    private AccountUtils accountUtils;

    @Autowired
    private RedisTemplate<String, Object> redisTemplate;
    /**
     * <p>添加用户</p>
     * <p>若成功则返回token,若不成功则返回null</p>
     * TODO 需要实现幂等性
     * @author liuyanbim
     * @Date 2019/11/21 13:56
     */

    public RpcBaseResponse register(SysUser user){
        //查询账号是否存在
        List<SysUser> accounts = this.list(new QueryWrapper<SysUser>().eq("ACCOUNT", user.getAccount()));
        if (accounts != null && accounts.size() > 0) {
            //throw new ServiceException(AuthExceptionEnum.USER_HAVE_REGISTER);
            return RpcBaseResponse.success(502,getMap("用户已注册"));
        } else {
            //获取一个随机的五位密码盐
            String salt = ToolUtil.getRandomString(5);
            user.setSalt(salt);
            //加密密码
            String md5HexPassword = ToolUtil.md5Hex(user.getPassword() + salt);
            user.setPassword(md5HexPassword);
            user.setCreateTime(new Date());
            if(this.save(user)){
                return RpcBaseResponse.wapeer(200,new HashMap<>(),true,"注册成功");
            }
            return RpcBaseResponse.error("注册失败");
        }
    }
    /**
     * 用户登录，登录成功返回token
     *
     * @author fengshuonan
     * @Date 2018/8/26 下午3:14
     */
    public RpcBaseResponse login(String username, String password, SystemMenu systemMenu) {

        //查询账号是否存在
        SysUser sysUser = null;
        List<SysUser> accounts = this.list(new QueryWrapper<SysUser>().eq("ACCOUNT", username));
        if (accounts != null && accounts.size() > 0) {
            sysUser = accounts.get(0);
        } else {
            //throw new ServiceException(AuthExceptionEnum.USER_NOT_FOUND);
            return RpcBaseResponse.success(504,"账号不存在",new HashMap<>());
        }

        //校验账号密码是否正确
        String md5Hex = ToolUtil.md5Hex(password + sysUser.getSalt());
        if (!md5Hex.equals(sysUser.getPassword())) {
            //throw new ServiceException(AuthExceptionEnum.INVALID_PWD);
            return RpcBaseResponse.success(505,"密码错误",new HashMap<>());
        }

        //生成token
        String jwtToken = accountUtils.setAccount(sysUser.getUserId());

        //token放入缓存
        LoginUser loginUser = new LoginUser();
        loginUser.setAccountId(sysUser.getUserId());
        BoundValueOperations<String, Object> opts = redisTemplate.boundValueOps(SystemConstants.LOGIN_USER_CACHE_PREFIX + jwtToken);
        opts.set(loginUser, SystemConstants.DEFAULT_LOGIN_TIME_OUT_SECS, TimeUnit.SECONDS);
        switch (systemMenu){
            case ANDROID:
            case IPHONE:
            case WEIXIN:
                AppUserWarpper a = new AppUserWarpper();
                if(sysUser.getBirthday()!=null){
                    Date date = sysUser.getBirthday();
                    a.setBirthday((date.getYear()+1900)+"-"+(date.getMonth() + 1)+"-"+date.getDate());
                }
                a.setHeadShot(sysUser.getAvatar());
                a.setGendar(sysUser.getSex());
                a.setLocation(sysUser.getLocation());
                a.setPhone(sysUser.getPhone());
                a.setName(sysUser.getName());
                a.setIslogin(true);
                return RpcBaseResponse.success(jwtToken,a);
            default:
                sysUser.setPassword("");
                sysUser.setCreateTime(null);
                sysUser.setSalt("");
                sysUser.setUpdateTime(null);
                sysUser.setUserId(null);
                return RpcBaseResponse.success(jwtToken,sysUser);
        }

    }

    /**
     * 校验token是否正确
     *
     * @author fengshuonan
     * @Date 2018/8/26 下午4:06
     */
    public boolean checkToken(String token) {
        //先校验jwt是否正确
        if (!jwtTokenUtil.checkToken(token)) {
            return false;
        }

        //校验缓存是否有token
        BoundValueOperations<String, Object> opts = redisTemplate.boundValueOps(SystemConstants.LOGIN_USER_CACHE_PREFIX + token);
        LoginUser loginUser = (LoginUser) opts.get();
        if (loginUser == null) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * 退出登录
     *
     * @author fengshuonan
     * @Date 2018/8/26 下午4:09
     */
    public void logout(String token) {
        redisTemplate.delete(SystemConstants.LOGIN_USER_CACHE_PREFIX + token);
    }

    /**
     * 获取登录用户通过token
     *
     * @author fengshuonan
     * @Date 2018/8/26 下午4:12
     */
    public LoginUser getLoginUserByToken(String token) {
        BoundValueOperations<String, Object> opts = redisTemplate.boundValueOps(SystemConstants.LOGIN_USER_CACHE_PREFIX + token);
        Object loginUser = opts.get();
        if (loginUser != null) {
            return (LoginUser) loginUser;
        } else {
            return null;
        }
    }
    public boolean isMobile(String str) {
        //^((13[0-9])|(14[5|7])|(15([0-3]|[5-9]))|(17([0,1,6,7,]))|(18[0-2,5-9]))\\d{8}$
        return Pattern.matches("^[1][3,4,5,8,7][0-9]{9}$", str);
    }
    public RpcBaseResponse updateAppUserInfo(AppUserWarpper userWarpper){
        UpdateWrapper<SysUser> queryWrapper = new UpdateWrapper<>();
        SysUser loginUser = new SysUser();
        if(userWarpper.getName()!=null&&!"".equals(userWarpper.getName())){
            loginUser.setName(userWarpper.getName());
        }
        if(userWarpper.getLocation()!=null&&!"".equals(userWarpper.getLocation())){
            loginUser.setLocation(userWarpper.getLocation());
        }
        if(userWarpper.getPhone()!=null&&!"".equals(userWarpper.getPhone())){
            if(!isMobile(userWarpper.getPhone())){
                return RpcBaseResponse.success(406,"手机号格式错误",new HashMap<>());
            }
            SysUser o = this.getOne(new QueryWrapper<SysUser>().eq("ACCOUNT",userWarpper.getPhone()));
            if(o!=null){
                if(!o.getPhone().equals(userWarpper.getPhone()))
                    return RpcBaseResponse.success(406,"手机号存在",new HashMap<>());
            }
            loginUser.setPhone(userWarpper.getPhone());
            loginUser.setAccount(userWarpper.getPhone());
        }
        if(userWarpper.getGendar()!=null&&!"".equals(userWarpper.getGendar())){
            loginUser.setSex(userWarpper.getGendar());
        }
        loginUser.setUserId(userWarpper.getId());
        Date date = null;
        try {
            if(userWarpper.getBirthday()!=null && !"".equals(userWarpper.getBirthday())){
                date = simpleDateFormat.parse(userWarpper.getBirthday());
            }
        } catch (ParseException e) {
            return RpcBaseResponse.success(406,"生日格式错误",new HashMap<>());
        }
        loginUser.setBirthday(date);

        if(userWarpper.getHeadShot()!=null&&!"".equals(userWarpper.getHeadShot())){
            loginUser.setAvatar(userWarpper.getHeadShot());
        }
        queryWrapper.eq("USER_ID", userWarpper.getId());
        if(this.update(loginUser,queryWrapper)){
            return RpcBaseResponse.success("更新成功");
        }else {
            return RpcBaseResponse.success(506,"更新失败",new HashMap<>());
        }
    }
    public RpcBaseResponse updatePassword(AppUserWarpper userWarpper,String token){
        SysUser loginUser = new SysUser();
        loginUser.setUserId(userWarpper.getId());
        SysUser loginUser2 = this.getById(userWarpper.getId());
        String md5Hex = ToolUtil.md5Hex(userWarpper.getPassword() + loginUser2.getSalt());
        loginUser.setPassword(md5Hex);
        UpdateWrapper<SysUser> queryWrapper = new UpdateWrapper<>();
        queryWrapper.eq("USER_ID", userWarpper.getId());
        if(this.update(loginUser,queryWrapper)){
            //删除redis中的token
            redisTemplate.delete(SystemConstants.LOGIN_USER_CACHE_PREFIX + token);
            return RpcBaseResponse.success("更新成功");
        }else {
            return RpcBaseResponse.success(506,"更新失败",new HashMap<>());
        }
    }

    /**
     * 更新简介
     * @param introduction
     * @param uid
     * @return
     */
    public RpcBaseResponse updateIntroduction(String introduction,long uid){
        UpdateWrapper<SysUser> queryWrapper = new UpdateWrapper<>();
        queryWrapper.set("introduction",introduction).eq("USER_ID", uid);
        if(this.update(queryWrapper)){
            return RpcBaseResponse.success("更新成功");
        }else {
            return RpcBaseResponse.success(506,"更新失败",new HashMap<>());
        }
    }
}
