/**
 * Copyright 2018-2020 stylefeng & fengshuonan (sn93@qq.com)
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.stylefeng.roses.system.modular.provider;

import cn.stylefeng.roses.core.util.FileUtil;
import cn.stylefeng.roses.kernel.model.api.AuthService;
import cn.stylefeng.roses.kernel.model.api.base.AppUserWarpper;
import cn.stylefeng.roses.kernel.model.api.base.RpcBaseResponse;
import cn.stylefeng.roses.kernel.model.auth.SysUser;
import cn.stylefeng.roses.kernel.model.enums.SystemMenu;
import cn.stylefeng.roses.kernel.model.exception.ServiceException;
import cn.stylefeng.roses.system.api.context.LoginUser;
import cn.stylefeng.roses.system.modular.service.SysUserService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Primary;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.util.HashMap;

/**
 * 鉴权服务的提供者
 *
 * @author fengshuonan
 * @date 2018-08-06-上午9:05
 */
@RestController
@Primary
@Api("鉴权服务")
@RefreshScope
public class AuthServiceProvider implements AuthService {
private static final Logger logger = LoggerFactory.getLogger(AuthServiceProvider.class);
    @Value("${saltice.introductionLimit}")
    private int introductionLimit;
    @Autowired
    private SysUserService sysUserService;
    /**(如果是windows下就"D://file/image")---本地路径，也就是文件保存的实际位置**/
    @Value("${saltice.upload.localDirectory}")
    private String localDirectory;

    /**jar包的映射路径**/
    @Value("${saltice.upload.staticMapping}")
    private String staticMapping;

    @Override
    @ApiOperation("注册用户")
    public RpcBaseResponse register(AppUserWarpper appUserWarpper) {
        SysUser sysUser = new SysUser();
        sysUser.setAccount(appUserWarpper.getPhone());
        sysUser.setPassword(appUserWarpper.getPassword());
        sysUser.setPhone(appUserWarpper.getPhone());
        sysUser.setName(appUserWarpper.getName());
        return sysUserService.register(sysUser);
    }

    @Override
    @ApiOperation("更新头像")
    public RpcBaseResponse updateAvatar(@RequestPart("avatar") MultipartFile f, @RequestParam("token") String token) {
        if (f.isEmpty()){
            return RpcBaseResponse.error("未获取到头像数据");
        }
        LoginUser loginUser  = sysUserService.getLoginUserByToken(token);
        if(loginUser!=null && loginUser.getAccountId()!=null){
            String avatar = FileUtil.saveImg(f,localDirectory);
            SysUser user = new SysUser();
            user.setUserId(loginUser.getAccountId());
            user.setAvatar(avatar);
            if(sysUserService.update(user,new QueryWrapper<SysUser>().eq("USER_ID", user.getUserId()))){
                return RpcBaseResponse.success("更新成功",avatar);
            }
            return RpcBaseResponse.error("更新失败");
        }
        return RpcBaseResponse.error("登录信息过期了，请重新登陆");
    }

    @Override
    @ApiOperation("登陆")
    public RpcBaseResponse login(@RequestParam("account") String account, @RequestParam("password") String password, @RequestParam("system") SystemMenu systemMenu) {
        return sysUserService.login(account, password,systemMenu);
    }

    @Override
    @ApiOperation("校验token")
    public boolean checkToken(@RequestParam("token") String token) {
        return sysUserService.checkToken(token);
    }

    @Override
    @ApiOperation("退出登陆")
    public void logout(@RequestParam("token") String token) {
        //直接删除redis中的缓存
        sysUserService.logout(token);
    }

    @Override
    @ApiOperation("通过token获取用户信息-getaway调用")
    public RpcBaseResponse getLoginUserByToken(@RequestParam("token") String token) {
        //从redis中获取缓存的用户对象信息
        LoginUser loginUser  = sysUserService.getLoginUserByToken(token);
        if(loginUser!=null && loginUser.getAccountId()!=null){
            SysUser user = sysUserService.getById(loginUser.getAccountId());
            user.setPassword("");
            user.setCreateTime(null);
            user.setSalt("");
            user.setUpdateTime(null);
            user.setUserId(null);
            return RpcBaseResponse.success("请求成功",user);
        }
        return RpcBaseResponse.success(500,"登陆信息失效",new HashMap<>());
    }

    @Override
    @ApiOperation("通过token获取用户信息-其他服务调用的")
    public SysUser getUserByToken(String token) {
        //从redis中获取缓存的用户对象信息
        LoginUser loginUser  = sysUserService.getLoginUserByToken(token);
        if(loginUser!=null && loginUser.getAccountId()!=null){
            SysUser user = sysUserService.getById(loginUser.getAccountId());
            user.setPassword("");
            user.setCreateTime(null);
            user.setSalt("");
            user.setUpdateTime(null);
            return user;
        }
        return null;
    }

    @Override
    @ApiOperation("通过token获取用户id")
    public Long getLoginUserIdByToken(String token) {
        //从redis中获取缓存的用户对象信息
        LoginUser loginUser  = sysUserService.getLoginUserByToken(token);
        if(loginUser!=null && loginUser.getAccountId()!=null){
            return loginUser.getAccountId();
        }
        return null;
    }

    @Override
    @ApiOperation("提供token更新用户信息")
    public RpcBaseResponse updateLoginUserByToken(@RequestParam("token") String token, AppUserWarpper userWarpper) {
        LoginUser loginUser  = sysUserService.getLoginUserByToken(token);
        if(loginUser==null){
            return RpcBaseResponse.success(555,"用户信息过期，请重新登陆",new HashMap<>());
        }
        userWarpper.setId(loginUser.getAccountId());
        return sysUserService.updateAppUserInfo(userWarpper);
    }

    /**
     * 更新密码
     * @param token
     * @param password
     * @return
     */
    @Override
    @ApiOperation("提供token更新密码")
    public RpcBaseResponse updatePasswordByToken(String token, String password) {
        AppUserWarpper userWarpper = new AppUserWarpper();
        LoginUser loginUser  = sysUserService.getLoginUserByToken(token);
        if(loginUser==null){
            return RpcBaseResponse.success(555,"用户信息过期，请重新登陆",new HashMap<>());
        }
        userWarpper.setId(loginUser.getAccountId());
        userWarpper.setPassword(password);
        return sysUserService.updatePassword(userWarpper,token);
    }

    @Override
    @ApiOperation("提供token更新用户简介")
    public RpcBaseResponse updateIntroductionByToken(String token, String introduction) {
        if(introductionLimit<50){
            introductionLimit = 50;
        }
        if(introduction.trim().length()>introductionLimit){
            return RpcBaseResponse.error("简介不能超过"+introductionLimit+"个字符");
        }
        LoginUser loginUser  = sysUserService.getLoginUserByToken(token);
        if(loginUser==null || loginUser.getAccountId()==null){
            return RpcBaseResponse.success(555,"用户信息过期，请重新登陆",new HashMap<>());
        }
        return sysUserService.updateIntroduction(introduction.trim(),loginUser.getAccountId());
    }
}
